package com.shen.app.rtos_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    final String SERVER_HOST = "http://140.123.103.21:406";
    final int AUTOMATIC_SLEEP = 3000;
    int state = 1;
    boolean automatic = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getState();

        final ImageView btn_add = (ImageView)findViewById(R.id.btnAdd);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (state < 4){setReq(++state);}
            }
        });
        final ImageView btn_sub = (ImageView)findViewById(R.id.btnSub);
        btn_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (state > 1){setReq(--state);}
            }
        });

        new Thread(new Runnable(){
            @Override
            public void run() {
                automatic  = true;
                while(automatic){
                    getState();
                    try {
                        Thread.sleep(AUTOMATIC_SLEEP);
                    }catch (Exception e){
                    }
                }
            }
        }).start();

        Switch sw = (Switch)findViewById(R.id.switch1);
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true){
                    btn_add.setVisibility(View.INVISIBLE);
                    btn_sub.setVisibility(View.INVISIBLE);
                    setReq(5);
                }else {
                    btn_add.setVisibility(View.VISIBLE);
                    btn_sub.setVisibility(View.VISIBLE);
                    //automatic = false;
                }
            }
        });
    }

    void setUI(){
        LinearLayout l1 = (LinearLayout)findViewById(R.id.l1);  //最底
        LinearLayout l2 = (LinearLayout)findViewById(R.id.l2);  //
        LinearLayout l3 = (LinearLayout)findViewById(R.id.l3);  //
        LinearLayout l4 = (LinearLayout)findViewById(R.id.l4);  //最高

        if (state == 4){
            l1.setBackground(this.getResources().getDrawable(R.drawable.bg_bottom_on));
            l2.setBackground(this.getResources().getDrawable(R.drawable.bg_mid_on));
            l3.setBackground(this.getResources().getDrawable(R.drawable.bg_mid_on));
            l4.setBackground(this.getResources().getDrawable(R.drawable.bg_top_on));
        }else if(state == 3){
            l1.setBackground(this.getResources().getDrawable(R.drawable.bg_bottom_on));
            l2.setBackground(this.getResources().getDrawable(R.drawable.bg_mid_on));
            l3.setBackground(this.getResources().getDrawable(R.drawable.bg_mid_on));
            l4.setBackground(this.getResources().getDrawable(R.drawable.bg_top));
        }else if(state == 2){
            l1.setBackground(this.getResources().getDrawable(R.drawable.bg_bottom_on));
            l2.setBackground(this.getResources().getDrawable(R.drawable.bg_mid_on));
            l3.setBackground(this.getResources().getDrawable(R.drawable.bg_mid));
            l4.setBackground(this.getResources().getDrawable(R.drawable.bg_top));
        }else if(state == 1){
            l1.setBackground(this.getResources().getDrawable(R.drawable.bg_bottom_off));
            l2.setBackground(this.getResources().getDrawable(R.drawable.bg_mid));
            l3.setBackground(this.getResources().getDrawable(R.drawable.bg_mid));
            l4.setBackground(this.getResources().getDrawable(R.drawable.bg_top));
        }
    }

    void getState(){
        OkHttpClient mOkHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(SERVER_HOST + "/index.php")
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback(){
            @Override
            public void onFailure(Request request, IOException e)
            {}

            @Override
            public void onResponse(final Response response) throws IOException
            {
                String res_state = response.body().string().toString();
                String led1 = String.valueOf(res_state.substring(1,2));
                String led2 = String.valueOf(res_state.substring(3,4));
                if (led1.equals("0") && led2.equals("0")){
                    state = 1;
                }else  if (led1.equals("0") && led2.equals("1")){
                    state = 2;
                }else if (led1.equals("1") && led2.equals("0")){
                    state = 3;
                }else if (led1.equals("1") && led2.equals("1")){
                    state = 4;
                }
                Log.d("state:", String.valueOf(state));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setUI();
                    }
                });

            }
        });
    }

    void setReq(int state){
        setUI();
        OkHttpClient mOkHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(SERVER_HOST + "/index.php?setState=" + String.valueOf(state))
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback(){
            @Override
            public void onFailure(Request request, IOException e)
            {}

            @Override
            public void onResponse(final Response response) throws IOException
            {}
        });
    }


}
