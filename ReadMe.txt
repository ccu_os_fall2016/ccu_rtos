This is project for Operating System course - Associate Professor Ren Song Ko

Please follow this guide

 - Pull code to your laptop
 - Install Visual Studio 2010
 - Go to FreeRTOS-Plus\Demo\RTOS_CCU. Open file RTOS_CCU.sln by VS 2010
 - Press F5 to run
 
Demo Video
https://youtu.be/_OquFrMAtPQ