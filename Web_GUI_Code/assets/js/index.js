// change the picture of light bulb
function change_light_bulb(state)
{

	switch(state)
	{
		case "^0,0^" : //1 25%
			document.getElementById('light-bulb_1').setAttribute('class','light-bulb off') ;
			
			document.getElementById('light-bulb_2').setAttribute('class','light-bulb off') ;
			
			break ;
		case "^0,1^" : //2 50%
			document.getElementById('light-bulb_1').setAttribute('class','light-bulb off') ;
			
			document.getElementById('light-bulb_2').setAttribute('class','light-bulb on') ;


			break ;
		case "^1,0^" : //3 75%
			document.getElementById('light-bulb_1').setAttribute('class','light-bulb on') ;
			document.getElementById('light-bulb_2').setAttribute('class','light-bulb off') ;
			
	 		
			break ;
		case "^1,1^" : //4 100%
	 		document.getElementById('light-bulb_1').setAttribute('class','light-bulb on') ;
			document.getElementById('light-bulb_2').setAttribute('class','light-bulb on') ;
			

			break ; 
	}

}

// modify the database.txt
function change_state()
{
    var state = document.getElementsByName('state')[0].value ;
    var choice = "state" ; 

    var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
	     change_light_bulb(state) ;

	   }
	 };
	xhttp.open("POST", "change.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send('choice=state&state='+state);




}	

// modify the configure txt
function change_mode()
{
    var mode = document.getElementsByName('mode')[0].value ;
    var choice = "mode" ; 

    var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
	    	show_hide_manual_div(mode) ;
	   }
	 };
	xhttp.open("POST", "change.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send('choice=mode&mode='+mode);


	
		
}

// show / hide the manual control block
function show_hide_manual_div(mode)
{
	if(mode == "1") //manual
		document.getElementById('manual_control_div').style.display = 'block' ;
	else //auto
		document.getElementById('manual_control_div').style.display = 'none' ;
}

// auto get the new state from database txt (when auto mode)
function page_refresh()
{
	
	//state
	var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
	      change_light_bulb(this.responseText);
	      change_option(this.responseText,0) ;
	       
	   }
	 };
	xhttp.open("POST", "read.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded") ;
	xhttp.send('choice=state');
	
}


// change the selected option
function change_option(state,mode)
{
			var option  = document.querySelectorAll('option') ;
			for(obj in option)
			{
				if(option[obj].value == state)
					option[obj].setAttribute('selected','true');
				if(option[obj].value == mode)
					option[obj].setAttribute('selected','true');
			}
			change_light_bulb(state) ;
		}
			



