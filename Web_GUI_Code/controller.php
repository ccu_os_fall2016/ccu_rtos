<?php
	$file = fopen('database.txt','r');
	$state = fgets($file) ;
	fclose($file) ;

	$file = fopen('configure.txt','r');
	$mode = fgets($file) ;
	fclose($file) ;

?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Light Control</title>
    <link rel="stylesheet" href="assets/css/find.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
  	<link rel="stylesheet" href="assets/css/style.css">
    <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>

      <style>
		body {
		  background: #46484b;
		}


		.left_bar{
			min-height: 100px ;
			margin-right:50px;
			margin-top:20px;
			background-color:rgba(255, 255, 255, 0.51);
			box-shadow:4px 4px 3px rgba(20%,20%,40%,0.5) ;
		}
		button{
			width:20%;
			margin-right:20px;
		}
		select{
			font-size:20px ;
		}
		
    </style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
	<script>
		window.onload = function(){
			change_option('<?=$state?>','<?=$mode?>') ;
			show_hide_manual_div('<?=$mode?>') ;
			setInterval(function(){
				var mode = document.getElementsByName('mode')[0].value ;
				if(mode == "0") //auto mode
					page_refresh() ;
			},5000) ;
		};

	</script>
</head>

<body>
	<div class="container" style="margin-top:5%">
		<div class="row">
			<div class="col-md-5 col-lg-5">
			<!-- Mode Block -->
				<div class="row left_bar" style="text-align:center">
					<div class="row">
						<span class="col-md-12 col-lg-12" style="text-align:center">
							<h3>Mode</h3>
						</span>
					</div>

					<div class="row">
						<div style="margin:20px">
							<form action="change.php" method="post">
								<select  name="mode" onchange="change_mode()">
									<option value="1">Manual</option>
									<option value="0">Auto</option>
								</select>
							</form>
						</div>
						
					</div>
				</div>
				<!-- Manual Control Block -->
				<div class="row left_bar" id="manual_control_div" style="padding:10px;display:none">
					<div class="row">
						<span class="col-md-12 col-lg-12" style="text-align:center">
							<h3>Manual Control</h3>
						</span>
					</div>
					<div class="row"  style="text-align:center;" >
						<p style="margin:20px;">
							<form action="change.php" method="post">
								<select  name="state"  onchange="change_state()" >
									<option value="^1,1^">100%</option>
									<option value="^1,0^">75%</option>
									<option value="^0,1^">50%</option>
									<option value="^0,0^">off</option>
								</select>
							</form>

						</p>
					</div>

				</div>


			</div>	

			<div class="col-md-5 col-lg-5">
				<table style="margin-top:100px;margin-left:60px">
					<tr>
						<td>
							<div  class="light-bulb off"  id="light-bulb_1" >
								<span class="bulb off"></span>
						 	 	<span class="bulb on"></span>
						   </div>
						</td>
						<td>
							 <div class="light-bulb off" id="light-bulb_2" >
							  	<span class="bulb off"></span>
						 		 <span class="bulb on"></span>
						  	 </div>
						</td>
						

					</tr>
				</table>
				
			</div>	
		</div>
	</div>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
    <script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/index.js"></script>
</body>
</html>
